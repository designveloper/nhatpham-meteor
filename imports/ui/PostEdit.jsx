import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import browserHistory from '../api/history';

export default class PostEdit extends Component {
  constructor(props) {
    super(props);
  }
  goBack(){
    browserHistory.goBack();
  }
  savePost(event) {
    event.preventDefault();
    let Post = {
      _id: this.props.location.state.post._id,
      title: this.refs.title.value,
      author: this.refs.author.value,
      content: this.refs.content.value,
    }
    console.log(Post);
    Meteor.call('post.update', Post, (error) => {
      if (error) alert('Something went wrong try again!!!');
      else {
        browserHistory.push('/');
      }
    })
  }
  render() {
    post = this.props.location.state.post;
    return (
      <div className="row">
        <form className="col s12" onSubmit={this.savePost.bind(this)} >
          <h3>Edit Post</h3>
          <div className="row">
            <div className="input-field col s6">
              <input type="text" placeholder="title" ref="title" className="validate" defaultValue={post.title} />
            </div>
            <div className="input-field col s6">
              <input type="text" placeholder="author" ref="author" className="validate" defaultValue={post.author} />
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <textarea type="text" placeholder="content" ref="content" className="materialize-textarea" defaultValue={post.content}/>
            </div>
            <div className="input-field col s6">
              <button type="submit" className="btn waves-effect waves-light" name="action">
                <i className="material-icons right"></i>Save
              </button>
              <button onClick={this.goBack.bind(this)} type="button" className="btn waves-effect waves-light" name="action">
                <i className="material-icons right"></i>back
              </button>

            </div>
          </div>
        </form>
      </div>
    )
  }
}