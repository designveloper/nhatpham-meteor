import React, { Component } from 'react';
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import Delete from 'material-ui/svg-icons/action/delete';
import Create from 'material-ui/svg-icons/content/create';
import FontIcon from 'material-ui/FontIcon';
import { red500, yellow500, blue500 } from 'material-ui/styles/colors';
import { Meteor } from 'meteor/meteor';
import browserHistory from '../api/history';

const iconStyles = {
  marginRight: 24,
};
const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: '100%',
    overflowY: 'auto',
  },
};

tilesData = [
  {
    _id: 1,
    img: 'kieutrinh.jpg',
    title: 'Breakfast',
    content: '',
    author: 'jill111',
  }
];

export default class List extends Component {
  deletePost(postId) {
    console.log(postId);
    
    Meteor.call('post.remove', postId, (error) => {
      if (error)
        alert('Something went wrong , try again');
      else
        alert('Post Deleted');
    })
  }
  editPost(post) {
    browserHistory.push('/edit', { post: post });
  }
  onItemClicked(post) {
    console.log(post);
    this.props.updateCurrentPost(post);
    this.props.showDetail();
  }
  render() {
    posts = this.props.posts;
    return (
      <div style={styles.root}>
        <GridList
          cellHeight={180}
          cols={3}
          style={styles.gridList}
        >
          <Subheader>Hot</Subheader>
          {posts.map((tile) => (

            <GridTile
              key={tile._id}
              title={tile.title}
              subtitle={<span>by <b>{tile.author}</b></span>}
              actionIcon={<div><IconButton onClick={this.editPost.bind(this, tile)} hoveredStyle={{ color: '#00ff00' }}><Create color="white" /></IconButton><IconButton hoveredStyle={{ color: '#00ff00' }}><Delete onClick={this.deletePost.bind(this, tile._id)} color="white" /></IconButton></div>}
              onClick={this.onItemClicked.bind(this, tile)}
            >
              <img src='kieutrinh.jpg' />
            </GridTile>
          ))}
        </GridList>
      </div>
    )
  }
}