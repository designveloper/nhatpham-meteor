import React from 'react';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import browserHistory from '../api/history.js'
const backward = (fn)=>{
  fn();
}
const Detail = ({post,showDetail}) => (
  <div className="container">
    <div className="row">
      <div className="col s12 m7">
        <Card>
          <CardHeader
            title={post.author}
            subtitle="Author"
            avatar="kieutrinh.jpg"
          />
          <CardMedia
            overlay={<CardTitle title={post.title} subtitle={post.author} />}
          >
            <img src="kieutrinh.jpg" alt="" />
          </CardMedia>
          <CardText>
            {post.content}
    </CardText>
          <CardActions>
            <FlatButton label="Like" />
            <FlatButton label="Buy" />
          </CardActions>
        </Card>
      </div>
      <div className="col s12-m5">
        <FlatButton label="Back" onClick={backward.bind(this,showDetail)}/>
      </div>
    </div>
  </div>
)

export default Detail;