import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import browserHistory from '../api/history';

export default class PostCreate extends Component {
  goBack() {
    browserHistory.goBack();
  }
  savePost(event) {
    event.preventDefault();
    let newPost = {
      title: this.refs.title.value,
      author: this.refs.author.value,
      content: this.refs.content.value,
      createAt: new Date(),
    }
    Meteor.call('post.insert', newPost, (error) => {
      if (error) alert('Something went wrong try again!!!');
      else {
        browserHistory.push('/');
      }
    })
    console.log(newPost);
  }
  render() {
    return (
      <div className="row">
        <form className="col s12" onSubmit={this.savePost.bind(this)} >
          <h3>Add a new Post</h3>
          <div className="row">
            <div className="input-field col s6">
              <input type="text" placeholder="title" ref="title" className="validate" />
            </div>
            <div className="input-field col s6">
              <input type="text" placeholder="author" ref="author" className="validate" />
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <textarea type="text" placeholder="content" ref="content" className="materialize-textarea" />
            </div>
            <div className="input-field col s6">

              <button type="submit" className="btn waves-effect waves-light" name="action">
                <i className="material-icons right"></i>Submit
              </button>
              <button onClick={this.goBack.bind(this)} type="button" className="btn waves-effect waves-light" name="action">
                <i className="material-icons right"></i>Back
              </button>

            </div>
          </div>
        </form>
      </div>
    )
  }
}