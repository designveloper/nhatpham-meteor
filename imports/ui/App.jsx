import React, { Component, PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import List from './List.jsx';
import Detail from './Detail.jsx';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router-dom';
import { createContainer } from 'meteor/react-meteor-data';
import { Posts } from '../api/post.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: false,
      currentPost: null
    }
    this.showDetail = this.showDetail.bind(this);
    this.updateCurrentPost = this.updateCurrentPost.bind(this);
  }
  showDetail() {
    this.setState({
      detail: !this.state.detail
    })
  }
  updateCurrentPost(post) {
    this.setState({
      currentPost: post
    })
  }
  renderPage() {
    if (this.state.detail == true) {
      return (
        <Detail showDetail={this.showDetail} post={this.state.currentPost} />
      )
    }
    else {
      return (<List posts={this.props.posts} showDetail={this.showDetail} updateCurrentPost={this.updateCurrentPost} />);
    }
  }
  render() {
    return (
      <MuiThemeProvider>
        <div className="">
          <AppBar
            title="My Blog"
            iconElementRight={<Link to="/create" className="btn waves-effect waves-light">Create Post</Link>}
            showMenuIconButton={false}
            style={{ backgroundColor: '#AB47BC' }}
          />
          <div className="row">
            <div className="col s12 m12">
              {this.renderPage()}
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}
App.propTypes = {
  posts: PropTypes.array.isRequired
}
export default createContainer(() => {
  Meteor.subscribe('posts');
  return {
    posts: Posts.find().fetch()
  }
}, App);