import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Posts } from '../imports/api/post.js';
Meteor.startup(() => {
  // code to run on server at startup
  Meteor.methods({
    'post.insert'(post) {
      Posts.insert(post);
    }
  });
  Meteor.methods({
    'post.update'(post) {
      Posts.update(post._id, { $set: post });
    }
  });
  Meteor.methods({
    'post.remove'(postId) {
      Posts.remove(postId);
    }
  });
  Meteor.methods({
    'post.find'() {
      return Posts.find().fetch();
    }
  });
  Meteor.methods({
    'post.findone'(id) {
      Post = Posts.findOne({_id:id});
      return Post;
    }
  });
});
