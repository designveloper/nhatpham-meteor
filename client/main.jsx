import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import ReactDOM from 'react-dom';
import { Route, Router, Switch } from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from '../imports/ui/App.jsx';
import PostCreate from '../imports/ui/PostCreate.jsx';
import PostEdit from '../imports/ui/PostEdit.jsx';
import Lost from '../imports/ui/Lost.jsx';
import Detail from '../imports/ui/Detail.jsx';
import browserHistory from '../imports/api/history';

injectTapEventPlugin();
Meteor.startup(() => {
  ReactDOM.render(
    <Router history={browserHistory}>
      <Switch>
        <Route exact path='/' component={App} />
        <Route path='/create' component={PostCreate} />
        <Route path='/edit' component={PostEdit} />
        <Route path='/detail' component={Detail} />
        <Route path='*' component={Lost} />
      </Switch>
    </Router>, document.getElementById('render-target'));
})